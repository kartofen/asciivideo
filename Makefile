CC=gcc
OPTION=-lv4l2
BIN=bin

all: iiv-demo iiv-capture iiv-playback

iiv-demo: iiv-demo.c common_v4l2.h bin
	$(CC) $< -o $(BIN)/$@ $(OPTION)

iiv-capture: iiv-capture.c common_v4l2.h bin
	$(CC) $< -o $(BIN)/$@ $(OPTION)

iiv-playback: iiv-playback.c bin
	$(CC) $< -o $(BIN)/$@

bin:
	mkdir -p $(BIN)
clean:
	rm -rf $(BIN)

.PHONY: build clean
