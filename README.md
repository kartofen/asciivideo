
# Table of Contents

1.  [AsciiVideo](#org3088b1f)
2.  [Features](#org49844e4)
        1.  [capture and playback <code>[0/3]</code>](#org7b4be38)
        2.  [optimize <code>[0/2]</code>](#org6f0ded7)
        3.  [make it more user friendly <code>[0/2]</code>](#org6cf876d)
3.  [asc(iiv)ideo file format](#org7b29317)
4.  [License](#orgbf60b9b)


<a id="org3088b1f"></a>

# AsciiVideo

Tools to create and load video files made out of ascii text.


<a id="org49844e4"></a>

# Features


<a id="org7b4be38"></a>

### TODO capture and playback <code>[0/3]</code>

-   [-] capture <code>[1/2]</code>
    -   [X] webcam capture given number of frames
    -   [ ] webcam capture more than 255 frames
-   [-] playback <code>[1/4]</code>
    -   [X] print all frames
    -   [ ] print frames in a given fram-rate
    -   [ ] add stop and continue
    -   [ ] add a video progres bar
-   [ ] convert from other video formats to iiv


<a id="org6f0ded7"></a>

### TODO optimize <code>[0/2]</code>

-   [-] optimized size <code>[1/3]</code>
    -   [X] use smaller types
    -   [ ] save only changed pixels
    -   [ ] idk
-   [-] optimize speed <code>[1/2]</code>
    -   [X] use smaller types (not fully done)
    -   [ ] idk


<a id="org6cf876d"></a>

### TODO make it more user friendly <code>[0/2]</code>

-   [ ] add -h for info
-   [ ] add useful error messages
-   [ ] add more info to readme


<a id="org7b29317"></a>

# asc(iiv)ideo file format

First byte is the size of reserved bytes (including itself).
Usually second, third and fourth are respectively x resolution, y resolution and frames.
Next bytes are all the density array indexes for every pixel for every fr


<a id="orgbf60b9b"></a>

# License

GNU GENERAL PUBLIC LICENSE Version 3

