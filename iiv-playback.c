#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <assert.h>

int X_RES = 0;
int Y_RES = 0;
int frames = 0;
size_t reserved_sz = 0;

static uint_fast8_t *buffer;
size_t buff_sz = 0;

char *density = "Ñ@#W$9876543210?!abc;:+=-,._     ";

void LoadFile(char *filename)
{
  char ch;
  FILE *fp;
  fp = fopen(filename, "r");
  if(fp == NULL) assert(0);

  fread(&reserved_sz, sizeof(char), 1, fp);
  assert(reserved_sz >= 4);
  fseek(fp, 1, SEEK_SET);
  
  unsigned char rsrvd[reserved_sz-1];
  fread(rsrvd, sizeof(char), reserved_sz-1, fp);
  X_RES =  (int) rsrvd[0] * 2;
  Y_RES =  (int) rsrvd[1] * 2;
  frames = (int) rsrvd[2];

  //  printf("%d, %d, %d, %d\n", reserved_sz, X_RES, Y_RES, frames);
  
  buff_sz = frames*X_RES*Y_RES;
  buffer = malloc(sizeof(uint_fast8_t)*buff_sz);
  
  fseek(fp, reserved_sz, SEEK_SET);
  fread(buffer, sizeof(char), sizeof(char)*buff_sz, fp);

  //printf("%d\n", buffer[0]);
  
  fclose(fp);
}
  
int main(int argc, char **argv)
{
  assert(argc == 2);
  
  // Initizalize
  //----------------------------------------------------
  LoadFile(argv[1]);

  struct winsize ws;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws);  
  assert(ws.ws_col >= X_RES);
  assert(ws.ws_row >= Y_RES);
  //----------------------------------------------------

  // Main
  //----------------------------------------------------
  for(int k=0; k<frames; k++)
  {
    for(int i=0; i<Y_RES; i++)
    {
      printf("\n");
      for(int j=0; j<X_RES; j++)
      {
	printf("%c ", density[buffer[k*(X_RES*Y_RES) + j + i*X_RES]]);
      }
    }
  }
  //----------------------------------------------------
  
  // Deinitialize
  //----------------------------------------------------
  free(buffer); // !!!
  return EXIT_SUCCESS;
  //----------------------------------------------------
}
