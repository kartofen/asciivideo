#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <sys/resource.h>
#include "common_v4l2.h"

#define CAM "/dev/video0"
#define X_RES 320
#define Y_RES 240

char *density = "Ñ@#W$9876543210?!abc;:+=-,._     ";

long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void SaveToFile(char *filename, unsigned char *data, size_t data_sz)
{
  FILE *fp;
  fp = fopen(filename, "w");
  if(fp == NULL) assert(0);

  fwrite(data, sizeof(char), data_sz, fp);
  
  fclose(fp);
}


int main(int argc, char **argv)
{
  assert(argc == 3);
  
  // make room in memory for the frames
  const rlim_t kStackSize = 2000L * 1024L * 1024L;   // min stack size = 2000MB
  struct rlimit rl;
  int result;

  result = getrlimit(RLIMIT_STACK, &rl); if (result != 0) assert(0);
  if (rl.rlim_cur < kStackSize)
  {
    rl.rlim_cur = kStackSize;
    result = setrlimit(RLIMIT_STACK, &rl);
    if (result != 0) assert(0);
  }
  
  // Initizalize
  //----------------------------------------------------
  size_t dnsty_sz = strlen(density);

  unsigned char frames = atoi(argv[2]);
  unsigned char *save_data[frames];
  for(int i=0; i<frames; i++)
    save_data[i] = (unsigned char *) malloc(sizeof(char)*X_RES*Y_RES);
  
  size_t reserved = 4;
  size_t dt_sz = frames*X_RES*Y_RES + reserved;
  unsigned char *newdata = malloc(sizeof(char) * dt_sz);
  
  CommonV4l2 common_v4l2;
  CommonV4l2_init(&common_v4l2, CAM, X_RES, Y_RES);
  //----------------------------------------------------
  
  // Main
  //----------------------------------------------------
  for(int k=0; k<frames; k++)
  {      
    CommonV4l2_update_image(&common_v4l2);
    char *data = CommonV4l2_get_image(&common_v4l2); //max 127; min -128
    size_t data_sz = CommonV4l2_get_image_size(&common_v4l2);
    
    for(int i=0; i<Y_RES; i++)
    {
      for(int j=0; j<X_RES; j++)
      {
	size_t pixelindex = (j + i * X_RES) * 3;
	char avg = (data[pixelindex+0] + //r
		   data[pixelindex+1] + //g
		   data[pixelindex+2]   //b
		   )/3;
	char char_i = map(avg, -128, 127, 0, dnsty_sz);
	save_data[k][j + i*X_RES] = char_i;
      }
    }
  }

  // turn data into array
  for(int i=0; i<frames; i++)
  {
    for(int j=0; j<X_RES*Y_RES; j++)
    {
      newdata[j + i*X_RES*Y_RES + reserved] = (char) save_data[i][j];
    }
  }

  newdata[0] = reserved;
  newdata[1] = X_RES/2;
  newdata[2] = Y_RES/2;
  newdata[3] = frames;

  for(int i=0; i<dt_sz; i++)
    printf("%d", newdata[i]);

  printf("\n%d\n", dt_sz);

  SaveToFile(argv[1], newdata, dt_sz);
  //----------------------------------------------------
  
  // Deinitialize
  //----------------------------------------------------
  for(int i=0; i<frames; i++) free(save_data[i]);
  free(newdata);
  CommonV4l2_deinit(&common_v4l2);
  return EXIT_SUCCESS;
  //----------------------------------------------------
}
