#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "common_v4l2.h"

#define CAM "/dev/video0"
#define X_RES 320
#define Y_RES 240

char *density = "Ñ@#W$9876543210?!abc;:+=-,._     ";

long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

int main(void)
{
  // Initizalize
  //----------------------------------------------------
  struct winsize ws;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws);
  int term_cols = ws.ws_col;
  int term_rows = ws.ws_row;

  size_t dnsty_sz = strlen(density);

  assert(term_cols >= X_RES);
  assert(term_rows >= Y_RES);

  CommonV4l2 common_v4l2;
  CommonV4l2_init(&common_v4l2, CAM, X_RES, Y_RES);
  //----------------------------------------------------

  // Main
  //----------------------------------------------------
  while(2)
  {
    CommonV4l2_update_image(&common_v4l2);
    char *data = CommonV4l2_get_image(&common_v4l2); //max 127; min -128
    size_t data_sz = CommonV4l2_get_image_size(&common_v4l2);

    for(int i=0; i<Y_RES; i++)
    {
      printf("\n");
      for(int j=0; j<X_RES; j++)
      {
	int pixelindex = (j + i * X_RES) * 3;
	int r = (int) data[pixelindex  ];
	int g = (int) data[pixelindex+1];
	int b = (int) data[pixelindex+2];
	int avg = (r + g + b)/3;
	int char_i = map(avg, -128, 127, 0, dnsty_sz);
	printf("%c ", density[char_i]);
      }
    }
  }
  //----------------------------------------------------

  // Deinitialize
  //----------------------------------------------------
  CommonV4l2_deinit(&common_v4l2);
  return EXIT_SUCCESS;
  //----------------------------------------------------
}
